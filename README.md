E-Golf4U Wordpress Plugin (DEPRECATED)
=====

Dit is de plugin die wordpress linkt aan een E-Golf4U omgeving.


Werking
----
De plugin werkt met een stuk client side code (De SSO functionaliteit) en een stuk server side wordpress code voor het afschermen van het leden gedeelte.
Client side hookt de plugin in op een login formulier die met de shortcode `[egolf4u_login_form]` gegeneert kan worden.
De SSO kan gelinkt worden aan het formulier door de javascript code aan te passen en de id van het login form in te voeren, zie deze uitleg hier: http://help.e-golf4u.nl/solution/categories/11485/folders/32049/articles/39639-e-golf4u-client-side-sso-v2.


Nodige aanpassingen
----

* Pas voor `js/sso_client.js` aan conform de uitleg in bovenstaand artikel.

* controleer of de message_placeholder div er is, hier worden berichten in getoond zoals b.v. inlog failed

* Pas in wordpress de instellingen aan in instellingen > E-Golf4U Members, hier staan 4 instellingen:
  
  _omgeving_: de url van de omgeving (b.v. demo.e-golf4u.nl)

  _leden pagina's_: de id's van de parent leden pagina. Alle pagina's die in de wordpress hierarchie (dus niet Menu's) hier onder vallen worden leden pagina's.

  _leden categorieŽn_: Zelfde als hierboven maar dan voor nieuws categorieen en de bijbehorende nieuws items.

  _Leden homepage_: De ID van de page waarheen geredirect wordt bij het inloggen


