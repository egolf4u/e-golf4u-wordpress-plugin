<?php  
    /* 
    Plugin Name: E-Golf4U Member & Single Sign On Plugin
    Plugin URI: http://www.e-golf4u.nl
    Description: Plugin to use the E-Golf4U SSO service to authenticate a user and login to wordpress
    Author: T. van Oorschot
    Version: 0.2
    Author URI: http://www.e-golf4u.nl
    */  

    require_once("class/EGolf4U.php");

    // Hooks
    add_action('wp_head', 'EGolf4U::add_header_javascript' );
	add_action('wp_footer', 'EGolf4U::add_footer_javascript' );
	add_action('filter_content', 'EGolf4U::is_page_allowed');

    //Shortcodes
    add_shortcode('egolf4u_login', 'EGolf4U::render_login_form' );
	add_shortcode('egolf4u_iframe', 'EGolf4U::render_iframe' );

    //Filters
    add_filter('wp_list_pages_excludes', 'EGolf4U::get_restricted_pages');
    add_filter('wp_list_categories', 'EGolf4U::get_restricted_categories');
    add_action('get_header', 'EGolf4U::check_restricted_page');
    

     function egolf4u_exclude_nav_items( $items, $menu, $args ) {
        if(EGolf4U::is_loggedin()){
            return $items; //Return gewoon alle items als we ingelogd zijn
        }else{
            $restriced_pages = EGolf4U::get_restricted_pages();

            // Iterate over the items to search and destroy
            foreach ( $items as $key => $item ) {
                if(in_array($item->object_id, $restriced_pages)){
                     unset( $items[$key] );
                }
            }
        }

        return $items;
    }

    if(!is_admin()){
        add_filter( 'wp_get_nav_menu_items', 'egolf4u_exclude_nav_items', null, 3 );
    }


    //Admin hooks 
    add_action('admin_menu', 'action_egolf4u_admin');

    function action_egolf4u_admin() {  
	    add_options_page("EGolf4USSO", "E-Golf4U SSO Plugin", 1, "EGolf4USSO", 'action_egolf4u_admin_option');
	}  

	function action_egolf4u_admin_option(){
		include('egolf4u_admin.php');  
	}
