<?php
	require_once("class/EGolf4UAdmin.php");

	if($_GET["create_pages"]){
		EGolf4UAdmin::create_pages();
		
		echo "Pagina's aangemaakt";
	}else{
		if($_POST['egolf4u_hidden'] == 'Y') {
			//Form data sent
			$egolf4u_host = $_POST['egolf4u_host'];
			$egolf4u_member_page = $_POST['egolf4u_member_page'];
			$egolf4u_member_category = $_POST['egolf4u_member_category'];
			$egolf4u_homepage = $_POST['egolf4u_homepage'];


			update_option('egolf4u_host', $egolf4u_host);
			update_option('egolf4u_member_page', $egolf4u_member_page);
			update_option('egolf4u_member_category', $egolf4u_member_category);
			update_option('egolf4u_homepage', $egolf4u_homepage);


			?>
			<div class="updated"><p><strong><?php _e('Instellingen opgeslagen.' ); ?></strong></p></div>
			<?php
		} else {
			$egolf4u_host = get_option('egolf4u_host');
			$egolf4u_member_page = get_option('egolf4u_member_page');
			$egolf4u_member_category = get_option('egolf4u_member_category');
			$egolf4u_homepage = get_option('egolf4u_homepage');
		}
	}


?>

<div class="wrap">
	<?php    echo "<h2>" . __('E-Golf4U Single Sign On Instellingen', 'egolf4u_dom' ) . "</h2>"; ?>

	<form name="egolf4u_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<input type="hidden" name="egolf4u_hidden" value="Y">
		<p><b><?php _e("Omgeving: " ); ?> </b> <input type="text" name="egolf4u_host" value="<?php echo $egolf4u_host; ?>" size="10"><?php _e(" eg: demo.e-golf4u.nl" ); ?></p>
		<p><b><?php _e("Leden pagina's: " ); ?> </b> <input type="text" name="egolf4u_member_page" value="<?php echo $egolf4u_member_page; ?>" size="10"></p>
		<p><b><?php _e("Leden categorieën: " ); ?> </b> <input type="text" name="egolf4u_member_category" value="<?php echo $egolf4u_member_category; ?>" size="10"></p>
		<p><b><?php _e("Leden homepage: " ); ?> </b> <input type="text" name="egolf4u_homepage" value="<?php echo $egolf4u_homepage; ?>" size="10"></p>

		<p class="submit">
		<input type="submit" name="Submit" value="<?php _e('Opslaan', 'egolf4u_dom' ) ?>" />
		</p>
	</form>
</div>

<hr />

<p>Klik hieronder op de tekst om de pagina's voor E-Golf4U aan te maken. <strong>LET OP</strong> Doe dit maar één keer!</p>
<p>
	<a href="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>&create_pages=1">Pagina's aanmaken</a>
</p>
	