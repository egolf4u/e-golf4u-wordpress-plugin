<?php

class EGolf4UAdmin {
    public static function create_pages(){
        $leden_parent = array(
            'post_title'    =>'Leden',
            'post_content'  => '[egolf4u_login]',
            'post_status'   => 'publish',
            'post_type'     => 'page'
        );

        //Maak eerst parent page aan
        $post_parent = wp_insert_post( $leden_parent );

        $pages = array(
            "profiel" => array(
                'post_title' => "Mijn Profiel",
                'post_content'  => '[egolf4u_iframe url="default/index"]'
            ),
            "wedstrijden" => array(
                'post_title' => "Wedstrijden",
                'post_content'  => '[egolf4u_iframe url="wedstrijd/index"]'
            ),
            "bestuur" => array(
                'post_title' => "Bestuur & Commissies",
                'post_content'  => '[egolf4u_iframe url="commissie/index"]'
            ),
            "competitie" => array(
                'post_title' => "Competities",
                'post_content'  => '[egolf4u_iframe url="competitie/index"]'
            ),
            "meerronde" => array(
                'post_title' => "Meerronde",
                'post_content'  => '[egolf4u_iframe url="meerronde/index"]'
            ),
            "vreemde kaarten" => array(
                'post_title' => "Kaarten invoeren",
                'post_content'  => '[egolf4u_iframe url="kaart/indexvreemd"]'
            ),
            "wachtwoord_vergeten" => array(
                'post_title' => "Wachtwoord vergeten",
                'post_content'  => '[egolf4u_iframe url="default/wachtwoordvergeten"]'
            ),
        );

        $member_homepage = null;
        foreach($pages as $key => $page){
            $page['post_author']  = 1;
            $page['post_parent']  = $post_parent;
            $page['post_status']  = 'publish';
            $page['post_type']  = 'page';

            $post_id = wp_insert_post( $page );

            if($key == "profiel"){
                $member_homepage = $post_id;
            }
        }

        update_option('egolf4u_member_page', $post_parent);
        update_option('egolf4u_homepage', $member_homepage);
       
    }   
}