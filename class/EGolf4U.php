<?php
if(!session_id()) {
	session_name("EGOLF4U_WORDPRESS");
	session_start();	
}

class EGolf4U {
	CONST CURL_METHOD_POST = "POST";
	CONST CURL_METHOD_GET = "GET";
	const TOKEN ="hdOUIDHSiuohkldbn29u390sn90hNJ";

	private $_egolf4uUrl;
	
	public function __construct($egolf4uOmgeving){
		$this->_egolf4uUrl = $egolf4uOmgeving;
	}

	private function request($url, $method=self::CURL_METHOD_GET, $queryString=""){
		$curl = curl_init();

		//De inlog actie is altijd POST, vandaar dat we een POST request opzetten
		if($method == self::CURL_METHOD_POST){
			curl_setopt($curl, CURLOPT_POST, 1);
		}

		if($method == self::CURL_METHOD_POST){
			
			if(!empty($queryString)){
				curl_setopt($curl, CURLOPT_POSTFIELDS, $queryString);	
			}
			
			//De url voor het remote inloggen van een relatie
			curl_setopt($curl, CURLOPT_URL, $this->_egolf4uUrl . '/leden/iframe/' . $url);
		}else{
			//echo $this->_egolf4uUrl . '/leden/iframe/' . $url;
			curl_setopt($curl, CURLOPT_URL, $this->_egolf4uUrl . '/leden/iframe/' . $url);
		}

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$output['content'] = curl_exec ($curl);
		$output['headers'] = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);

		return $output;
	}

	public static function login(){
		$_SESSION["EGOLF4U_LOGGED_IN"] = true;
	}

	public static function logout(){
		$_SESSION = array();

		if (isset($_COOKIES[session_name()])) { 
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', 1, $params['path'], $params['domain'], $params['secure'], isset($params['httponly']));
		}
	}

	public static function is_loggedin(){
		if($_SESSION["EGOLF4U_LOGGED_IN"] == true){
			return true;
		}else{
			return false;
		}
	}

	public function validate($username, $password){
		$serverOutput = $this->request('default/remotelogin/check_credentials/?gebruikersnaam=' . $username . '&wachtwoord=' . $password);

		$response = json_decode($serverOutput['content']);

		return ($response->status == "ok");
	}

	public function get_relatie_id($username, $password){
		$serverOutput = $this->request('default/remotelogin/check_credentials/?gebruikersnaam=' . $username . '&wachtwoord=' . $password);

		$response = json_decode($serverOutput['content']);

		if($response->status == "ok"){
			return $response->relatie_id;
		}else return null;
	}

	public function get_relatie($id){
		$serverOutput = $this->request('relatie/show.json/' . $id, self::CURL_METHOD_GET, "", self::TOKEN);

		$response = json_decode($serverOutput['content']);
		
		return $response;
	}

	public function is_page_allowed($post){
		//if(!self::is_loggedin() && in_array($post->ID, self::get_restricted_pages()) ){
			////self::render_login_form();
		//}
	}

	//Return een array met pagina's die niet getoond mogen worden als je niet ingelogd bent
	public static function get_restricted_pages(){


		if(!self::is_loggedin()){
			$leden_pagina = get_option('egolf4u_member_page');
			$excluded_pages = array();
			
			$my_wp_query = new WP_Query();
			$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => -1));
			
			$paginas = explode(",", $leden_pagina);
			if(sizeof($paginas) > 0){
				foreach($paginas as $pagina){
					//$excluded_pages[] = $pagina;

					//Kijk of deze pagina ook kinderen heeft
					$children = get_page_children($pagina, $all_wp_pages);

					if(sizeof($children) > 0){
						foreach($children as $child){
							$excluded_pages[] = $child->ID;
						}
					}
				}
			}

			return $excluded_pages;
		}else{
			return array();
		}
	}

	public function get_restricted_categories()
	{
		if(!self::is_loggedin()){
			$cats = explode(",", get_option('egolf4u_member_category'));

			return $cats;
		}
	}

	public static function check_restricted_page ()
	{

		if(!self::is_loggedin()){

			global $post;
			$query_var_cat = get_query_var('cat');

			//Als het een pagina is dan checken op restricted pages
			if(is_page($post->ID)){
				if(in_array($post->ID, self::get_restricted_pages())){
					header('Location: ' . home_url());
					exit();
				}
			}elseif(!empty($query_var_cat)){//check voor categorie pagina
				$restricted_categories = self::get_restricted_categories();
				if(in_array($query_var_cat, $restricted_categories)){
					header('Location: ' . home_url());
					exit();
				}
			}else{ //anders op categorien
				$categories = get_the_category( $post->ID );
				$category_check = array();
				if(sizeof($categories) > 0){
					foreach($categories as $category){
						$category_check[] = (int) $category->term_id;
					}
				}
				$restricted_categories = self::get_restricted_categories();

				foreach($category_check as $cat_check){
					if(in_array($cat_check, $restricted_categories)){
						header('Location: ' . home_url());
						exit();
					}
				}
			}
		}

		return false;
	}

	//
	// Static functions voor het gebruik in shortcodes en hooks
	//
	public static function render_iframe($atts){
		//De host = de omgeving van de club	
		$egolf4u_host = get_option('egolf4u_host');  

		$iframe = '<script src="http://' . $egolf4u_host . '/leden/iframe/js/resizehandler.js"></script>
				   <iframe id="egolf4u_iframe" src="http://' . $egolf4u_host . '/leden/iframe/' . $atts["url"] . '" style="width: 100%;"></iframe>';

		return $iframe;
	}


	public static function render_login_form(){
		$login_action =  get_bloginfo('url') . '?plugin=egolf4u&action=login';
		return '
			<div id="egolf4u_login_message"></div>
			<form method="post" action="' . $login_action . '" id="egolf4u_login_form" class="form-horizontal">
				<div class="form-group">
					<label for="egolf4u_login_username" class="col-sm-2 control-label">Gebruikersnaam:</label> 
					<div class="col-sm-4">
						<input type="text" name="egolf4u_login_username" id="egolf4u_login_username" class="form-control" placeholder="Voer uw persoonscode in"/>
					</div>
				</div>
				<div class="form-group">
					<label for="egolf4u_login_password" class="col-sm-2 control-label">Wachtwoord:</label> 
					<div class="col-sm-4">
						<input type="password" name="egolf4u_login_password" id="egolf4u_login_password"  class="form-control"/>
					</div>
				</div>
				<div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <input type="submit" name="egolf4u_login_submit" id="egolf4u_login_submit" value="Login" class="btn btn-default" />
				    </div>
			  	</div>
			</form>

		';
	}

	public static function add_header_javascript(){
		//echo '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>';
		echo '<script src="http://api.e-golf4u.nl/static/sso.2.2.js"></script>';
		
		//Client javascript
		$plugin_url = plugins_url() . '/egolf4u-wordpress-plugin';

		echo '<script src="' . $plugin_url . '/js/sso_client.js"></script>';
	}

	public static function add_footer_javascript(){
		$plugin_url = plugins_url() . '/e-golf4u-wordpress-plugin';
		$host_url = get_option('egolf4u_host');
		echo '
			<script>
				var egolf4u_plugin_url = "' . $plugin_url . '";
				var egolf4u_host_url = "' . $host_url . '";
			</script>
		';
	}

	public static function handle_login()
	{
		$egolf4u_host = get_option('egolf4u_host');  
		
		$EGolf4U = new EGolf4U($egolf4u_host);

		try {
			$username = $_POST['egolf4u_login_username'];
			$password = $_POST['egolf4u_login_password'];

			if(empty($username) || empty($password)) throw new Exception('Gebruikersnaam en/of wachtwoord niet ingevuld');
			
			if($EGolf4U->validate($username, $password)){

				$relatie_id = $EGolf4U->get_relatie_id($username, $password);
				$relatie = $EGolf4U->get_relatie($relatie_id);

				$EGolf4U->login();

				$_SESSION["EGOLF4U_NAAM"] = $relatie->voorletters . " " . (!empty($relatie->voorvoegsels) ? $relatie->voorvoegsels . " " : "") . $relatie->achternaam;

				header('Location: ' . get_permalink(get_option('egolf4u_homepage')));

			}else{
				wp_die("De gebruikersnaam en/of wachtwoord konden niet bij E-Golf4U gevalideerd worden.");
			}
		} catch (Exception $e) {
			die(print_r($e, true));
		}
	}

	public static function handle_logout()
	{
			$egolf4u_host = get_option('egolf4u_host');  

			$EGolf4U = new EGolf4U($egolf4u_host);
			$EGolf4U->logout();

			wp_redirect(home_url());
	}
	
}
